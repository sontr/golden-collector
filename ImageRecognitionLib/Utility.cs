﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;

namespace ImageRecognitionLib
{
    public static class ScreenUtility
    {
        public static Bitmap CaptureScreenshot()
        {
            Rectangle bounds = Screen.GetBounds(Point.Empty);
            Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                //graphics.DrawImage(bitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height));
            }

            //bitmap.Save(@"D:\Temp\screenshotNew.png", System.Drawing.Imaging.ImageFormat.Png);

            return bitmap;
        }

        public static Point GetCenter(Rectangle rectangle)
        {
            return new Point(
                (rectangle.X + rectangle.Width / 2),
                (rectangle.Y + rectangle.Height / 2));
        }

        public static void SimulateClick(Point coord)
        {
            Rectangle screenBounds = Screen.GetBounds(Point.Empty);
            int scalePoint = 65535;
            double x = (double)coord.X / screenBounds.Width * scalePoint;
            double y = (double)coord.Y / screenBounds.Height * scalePoint;

            InputSimulator inputSimulator = new InputSimulator();
            inputSimulator.Mouse.MoveMouseTo(x, y);
            inputSimulator.Mouse.Sleep(100);
            inputSimulator.Mouse.LeftButtonClick();
        }
    }
}
