﻿using Accord.Imaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageRecognitionLib
{
    public class ImageMatcher
    {
        public ImageMatcher(float similarityThreshold = 0.9f)
        {
            this.SimilarityThreshold = similarityThreshold;
        }

        public float SimilarityThreshold { get; private set; }

        public ReadOnlyCollection<TemplateMatch> TemplateMatches { get; private set; }

        public Bitmap HighlightImage(Bitmap image, Bitmap templateImage)
        {
            // The ExhaustiveTemplateMatching class also can be used to get similarity level between two image
            // of the same size, which can be useful to get information about how different/similar are images:

            // Create template matching algorithm's instance
            // Use zero similarity threshold will make algorithm provide anything
            ExhaustiveTemplateMatching tm = new ExhaustiveTemplateMatching(this.SimilarityThreshold);

            // Compare two images
            TemplateMatch[] matches = tm.ProcessImage(image, templateImage);

            // Save the matches
            this.TemplateMatches = new ReadOnlyCollection<TemplateMatch>(matches);

            // Highlight found matches
            Bitmap highlightedImage = new Bitmap(image);
            BitmapData data = highlightedImage.LockBits(
                new Rectangle(0, 0, highlightedImage.Width, highlightedImage.Height),
                ImageLockMode.ReadWrite,
                highlightedImage.PixelFormat);

            foreach (TemplateMatch match in matches)
            {
                Drawing.Rectangle(data, match.Rectangle, Color.OrangeRed);
                //Drawing.FillRectangle(data, match.Rectangle, Color.Yellow);
            }
            highlightedImage.UnlockBits(data);

            return highlightedImage;
        }
    }
}
