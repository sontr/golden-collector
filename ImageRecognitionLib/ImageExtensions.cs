﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageRecognitionLib
{
    public static class ImageExtensions
    {
        public static Bitmap ConvertToFormat(this Image image, PixelFormat format)
        {
            Bitmap imageCopy = new Bitmap(image.Width, image.Height, format);

            using (Graphics graphics = Graphics.FromImage(imageCopy))
            {
                graphics.DrawImage(image, new Rectangle(0, 0, imageCopy.Width, imageCopy.Height));
            }

            return imageCopy;
        }
    }
}
