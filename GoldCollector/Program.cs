﻿using ImageRecognitionLib;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

namespace GoldCollector
{
    class Program
    {
        static int Main(string[] args)
        {
            //string imagePath = @"D:\_MyDocuments\Documents\Source\Repos\GoldCollector\GoldCollector\Images\DragonMania.png";
            //string imagePath = @"D:\Temp\screenshotNew.bmp";
            string templateImagePath = @"D:\_MyDocuments\Documents\Source\Repos\GoldCollector\GoldCollector\Images\DragonMania-Gold.png";

            //Bitmap targetImage = (Bitmap)Bitmap.FromFile(imagePath);
            Bitmap targetImage = ScreenUtility.CaptureScreenshot();
            Bitmap templateImage = (Bitmap)Image.FromFile(templateImagePath);

            // Convert images to correct format
            targetImage = targetImage.ConvertToFormat(PixelFormat.Format24bppRgb);
            templateImage = templateImage.ConvertToFormat(PixelFormat.Format24bppRgb);

            ImageMatcher matcher = new ImageMatcher();
            matcher.HighlightImage(targetImage, templateImage);

            foreach (var match in matcher.TemplateMatches)
            {
                Point centerCoord = ScreenUtility.GetCenter(match.Rectangle);

                ScreenUtility.SimulateClick(centerCoord);
            }

            int matchedCount = matcher.TemplateMatches.Count;

            EventLog.WriteEntry("Application", string.Format("{0} point(s) clicked.", matchedCount));
            Console.WriteLine(string.Format("{0} point(s) clicked.", matchedCount));

            return matchedCount;
        }
    }
}
